// BauTek Call Notifier for the Callmaster IV phones
// Adam J. Bauman, September 2015

int pinLightSens = 0;
int pinAlert = 3;
int pinLED = 13;

int lightTarget = 100;

int alertStatus = 0;
long alertTime = 0;
long alertOffTime = 0;

void setup() {
  Serial.begin(9600);
  pinMode(pinAlert, OUTPUT);
  pinMode(pinLED, OUTPUT);

  digitalWrite(pinAlert, 1);
  digitalWrite(pinLED, 1);
  delay(200);
  digitalWrite(pinAlert, 0);
  digitalWrite(pinLED, 0);
  delay(200);
  digitalWrite(pinAlert, 1);
  digitalWrite(pinLED, 1);
  delay(200);
  digitalWrite(pinAlert, 0);
  digitalWrite(pinLED, 0);
}

void loop() {
  int lightValue = analogRead(pinLightSens);
  long currentTime = millis();
  Serial.print("LightValue: ");Serial.println(lightValue);
  Serial.print("TriggerStatus: ");Serial.println(alertStatus);
  Serial.print("TriggerTime: ");Serial.println(alertTime);
  Serial.print("TriggerOffTime: ");Serial.println(alertOffTime);

  if (lightValue > lightTarget && alertStatus == 0) {
    // Alert wasn't running and a call is coming in
    StartAlert(); 
  } else if (currentTime > alertOffTime) {
    // Timer has run, we don't want it running the whole call
    StopAlert();
    
    if (currentTime > alertOffTime && lightValue < lightTarget) {
      // The call is over, reset the alarm trigger
      ResetAlert();
    }
  }
  
  Serial.println("-----------");
  delay(100);
}

//
// Triggers the alarm output
//
void StartAlert() {
  Serial.println("StartAlarm");
  alertTime = millis();
  alertOffTime = alertTime + 6000;
  alertStatus = 1;
  OutputPulse();
}

//
// Kills alert output
//
void StopAlert() {
  Serial.println("StopAlarm");
  digitalWrite(pinAlert, 0);
  digitalWrite(pinLED, 0);
}

//
// Re-arms the trigger for the alert
//
void ResetAlert() {
  Serial.println("ResetTrigger");
  alertStatus = 0;
  alertTime = 0;
  alertOffTime = 0;
  StopAlert();
}

//
// Pulses the output and onboard LED
//
void OutputPulse() {
  while (millis() < alertOffTime) {
    digitalWrite(pinAlert, 1);
    digitalWrite(pinLED, 1);
    delay(200);
    digitalWrite(pinAlert, 0);
    digitalWrite(pinLED, 0);
    delay(200);
  }
}

